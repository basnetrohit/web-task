# Sqreen Web-Task 

## Page design explaination
* [Header](#Header)
* [Customer](#Customer carousel)
* [Blocks](#Blocks)
* [Setup](#setup)
* [Features](#features)
* [Status](#status)
* [Inspiration](#inspiration)
* [Contact](#contact)

## Header
Header is build same as RASP page.

## Customer carousel
Customer carousel is build with just list of customer logos as RASP page but there is no carousel implemented there.So kept it simple.

## Blocks
Mirror design for the existing ‘Protect applications’ block
## Customer quote
Customer quote build with responsive design taking the reference from given screenshot and also modified little.
It is in three different design. 

## CTA for a Free Trial
same design as homepage

## Why Sqreen
same design as homepage


## Resources
same design as homepage

## Footer
Same design as homepage
